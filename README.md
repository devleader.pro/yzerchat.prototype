# Yzer Chat

React + Onsen UI + Cordova

#### Начало работы:

```
$ git clone git@gitlab.rusoft.pro:yzer/yzer-chat-web-2.git
$ cd yzer-chat-web-2
$ npm i
```

### Web:

`npm start` - старт приложения (webpack-dev-server).

`npm run build` - сборка приложения в папку **www**.

### Android:

Потребуется [Android SDK](http://developer.android.com/sdk/index.html) (входит в комплект Android Studio).

При первом запуске AS будет скачивание SDK и прочего, можно сразу создать пустой проект и создать устройство.

`npm run build` - сборка Web приложения в папку **www**.

`cordova build android` - сборка Andoind приложения (**.apk**) без подписи.

`cordova emulate android` - запускает сборку из **www** на эмуляторе.

В Chrome можно осуществлять отладку запущенного (на эмуляторе или через кабель) приложения: <chrome://inspect/#devices>.

---

[Onsen UI](https://onsen.io/)
[Документация Сordova](https://cordova.apache.org/docs/ru/latest/guide/cli/)
