import { format, isToday, isYesterday } from 'date-fns'

export const lastSeenFormat = timestamp => {
  let now = Date.now()
  let date = new Date(timestamp)

  let day = date.getDate()
  let month = date.getMonth()
  let hours = date.getHours()
  let min = date.getMinutes()
  month = month.toString().length === 2 ? month : '0' + month
  min = min.toString().length === 2 ? min : '0' + min
  return `${day}.${month} в ${hours}:${min}`
}

export const getLastMsgDateTime = timestamp => {
  if (isToday(timestamp)) {
    return format(timestamp, 'HH:mm')
  }
  if (isYesterday(timestamp)) {
    return 'Вчера'
  }
  return format(timestamp, 'dd.MM.yy')
}

export function throttle(func, ms) {
  let isThrottled = false,
    savedArgs,
    savedThis

  function wrapper() {
    if (isThrottled) {
      // (2)
      savedArgs = arguments
      savedThis = this
      return
    }

    func.apply(this, arguments) // (1)

    isThrottled = true

    setTimeout(function() {
      isThrottled = false // (3)
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs)
        savedArgs = savedThis = null
      }
    }, ms)
  }

  return wrapper
}

export function debounce(f, ms) {
  let isCooldown = false

  return function() {
    if (isCooldown) return

    f.apply(this, arguments)

    isCooldown = true

    setTimeout(() => (isCooldown = false), ms)
  }
}

export function searchInArrayByObjectProperty(array, term, field = 'name') {
  if (term.length === 0) {
    return array
  }

  return array.filter(obj => {
    return obj[field].toLowerCase().indexOf(term.toLowerCase()) > -1
  })
}

export const langs = [
  {
    inits: 'ar',
    name: 'Арабский',
  },
  {
    inits: 'zh',
    name: 'Китайский',
  },
  {
    inits: 'en',
    name: 'Английский',
  },
  {
    inits: 'fa',
    name: 'Персидский (Фарси)',
  },
  {
    inits: 'fr',
    name: 'Французский',
  },
  {
    inits: 'de',
    name: 'Немецкий',
  },
  {
    inits: 'hi',
    name: 'Хинди',
  },
  {
    inits: 'id',
    name: 'Индонезийский',
  },
  {
    inits: 'ja',
    name: 'Японский',
  },
  {
    inits: 'kk',
    name: 'Казахский',
  },
  {
    inits: 'ko',
    name: 'Корейский',
  },
  {
    inits: 'pl',
    name: 'Польский',
  },
  {
    inits: 'pt',
    name: 'Португальский',
  },
  {
    inits: 'ru',
    name: 'Русский',
  },
  {
    inits: 'es',
    name: 'Испанский',
  },
  {
    inits: 'tr',
    name: 'Турецкий',
  },
  {
    inits: 'uk',
    name: 'Украинский',
  },
  {
    inits: 'ur',
    name: 'Урду',
  },
]
