import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from './reducer'

const configureStore = preloadedState => {
  const logger = createLogger({
    diff: true,
    collapsed: true,
  })

  const enhancer = compose(applyMiddleware(thunk, logger))

  const store = createStore(rootReducer, preloadedState, enhancer)

  return store
}

export default configureStore
