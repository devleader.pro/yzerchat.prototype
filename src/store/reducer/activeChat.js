const initialState = { translatedManually: { opened: false } }

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_MESSAGE':
      return { ...state, messages: [...state.messages, action.payload] }

    case 'CLOSE_ACTIVE_CHAT': {
      return { ...state, opened: false }
    }

    case 'SET_ACTIVE_CHAT': {
      const { id, messages, translate, paginationToken } = action.payload

      return {
        id,
        messages,
        translate,
        paginationToken,
        translatedManually: { ...state.translatedManually },
        opened: true,
        typing: false,
      }
    }

    case 'UPDATE_ACTIVE_CHAT': {
      const { messages, paginationToken } = action.payload
      return { ...state, messages, paginationToken }
    }

    case 'TOGGLE_MESSAGE_SHOW_TRANSLATE': {
      const msgIndex = state.messages.findIndex(msg => {
        return msg.id === action.payload.msgId
      })
      const msg = { ...state.messages[msgIndex] }
      msg.showTranslate = action.payload.showTranslate

      return {
        ...state,
        messages: [
          ...state.messages.slice(0, msgIndex),
          msg,
          ...state.messages.slice(msgIndex + 1),
        ],
        translatedManually: {
          ...state.translatedManually,
          [msg.id]: msg.showTranslate,
        },
      }
    }

    case 'SET_ACTIVE_CHAT_MESSAGE_STATUS': {
      const msgIndex = state.messages.findIndex(msg => {
        return msg.id === action.payload.msgId
      })
      const msg = { ...state.messages[msgIndex] }
      msg.status = action.payload.status

      return {
        ...state,
        messages: [
          ...state.messages.slice(0, msgIndex).map(msg => {
            return { ...msg, status: action.payload.status }
          }),
          msg,
          ...state.messages.slice(msgIndex + 1),
        ],
      }
    }

    case 'LOADING_HISTORY': {
      return { ...state, loading: action.payload }
    }

    case 'USER_TYPING': {
      const { roomId, typing, name } = action.payload

      if (roomId === state.id) {
        return { ...state, typing: typing ? name + ' пишет...' : false }
      }

      return { ...state }
    }

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
