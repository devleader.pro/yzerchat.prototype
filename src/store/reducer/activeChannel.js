import cat from '../../images/posts/cat.jpg'
import hajabusa from '../../images/posts/hajabusa.jpg'
import neuro from '../../images/posts/neuro.jpg'
import news from '../../images/posts/news.png'
const initialState = {
  id: 200,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_CHANNEL_MESSAGE':
      return { ...state, messages: [...state.messages, action.payload] }

    // case 'RESET_STATE': {
    //   return initialState
    // }
    //

    case 'SET_ACTIVE_CHANNEL': {
      return { id: action.id }
    }

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
