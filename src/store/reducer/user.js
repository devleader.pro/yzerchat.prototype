const initialState = {
  isLoggedIn: null,
  privacyAccepted: localStorage.getItem('privacyAccepted'),
  profile: {
    id: null,
    name: null,
    email: null,
    phone: null,
    avatar: null,
  },
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        loginPhone: action.payload.phone,
      }

    case 'LOGIN_CONFIRM_SUCCESS':
      return {
        ...state,
        isLoggedIn: true,
      }

    case 'LOGOUT_SUCCESS':
      return {
        ...state,
        isLoggedIn: false,
      }

    case 'SET_IS_LOGGED_IN':
      return {
        ...state,
        isLoggedIn: action.payload,
      }

    case 'ACCEPT_PRIVACY':
      return {
        ...state,
        privacyAccepted: action.payload,
      }

    case 'UPDATE_PROFILE':
      return {
        ...state,
        profile: { ...state.profile, ...action.payload },
      }

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
