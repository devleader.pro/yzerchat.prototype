const initialState = JSON.parse(localStorage.getItem('yzch_translates')) || {}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'TRANSLATE_TEXT':
      const { msgId, lang, translatedText } = action.payload

      return { ...state, [lang]: { ...state[lang], [msgId]: translatedText } }

    default:
      return state
  }
}
