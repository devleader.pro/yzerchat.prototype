const initialState = []

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SYNC_CHATS':
      return [...state, ...action.payload]

    case 'UPDATE_LAST_MESSAGE': {
      const { roomId, lastMessage, incCount } = action.payload

      const chat = state.find(item => {
        return item.id === roomId
      })

      const chatIndex = state.findIndex(item => {
        return item.id === roomId
      })

      const newItem = {
        ...chat,
        lastMessage,
      }

      if (incCount) {
        newItem.unreadMessagesCount = chat.unreadMessagesCount + 1
      }

      return [
        ...state.slice(0, chatIndex),
        newItem,
        ...state.slice(chatIndex + 1),
      ]
    }

    case 'SET_CHAT_TRANSLATE_SETTINGS': {
      const { id, translate } = action.payload

      const chatIndex = state.findIndex(item => {
        return item.id === id
      })

      const chat = state[chatIndex]

      const newItem = { ...chat, translate }

      return [
        ...state.slice(0, chatIndex),
        newItem,
        ...state.slice(chatIndex + 1),
      ]
    }

    case 'SET_CHAT_LAST_MESSAGE_STATUS': {
      const { msgId, roomId, status } = action.payload

      const chatIndex = state.findIndex(item => {
        return item.id === roomId
      })

      const chat = state[chatIndex]

      if (chat.lastMessage?.id === msgId) {
        const newItem = {
          ...chat,
          lastMessage: { ...chat.lastMessage, status: status },
        }

        return [
          ...state.slice(0, chatIndex),
          newItem,
          ...state.slice(chatIndex + 1),
        ]
      } else {
        return state
      }
    }

    case 'UPDATE_UNREAD_ROOM_MESSAGES_COUNT': {
      const { roomId, count } = action.payload

      const chatIndex = state.findIndex(item => {
        return item.id === roomId
      })

      const chat = state[chatIndex]

      const newItem = { ...chat, unreadMessagesCount: count }

      return [
        ...state.slice(0, chatIndex),
        newItem,
        ...state.slice(chatIndex + 1),
      ]
    }

    case 'USER_TYPING': {
      const { roomId, typing } = action.payload

      const chatIndex = state.findIndex(item => {
        return item.id === roomId
      })

      const chat = state[chatIndex]

      const newItem = { ...chat, typing }

      return [
        ...state.slice(0, chatIndex),
        newItem,
        ...state.slice(chatIndex + 1),
      ]
    }

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
