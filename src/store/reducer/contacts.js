const initialState = []

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SYNC_CONTACTS':
      return [...state, ...action.payload]

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}

/**
{
    id: 1,
    name: 'Авдотий Ластнеймович',
    lastSeen: 12345678905322,
    avatar:
      'https://gravatar.com/avatar/ae88c301f8a2abf87738f366d46711f2?s=200&d=robohash&r=g',
    online: false,
  },

*/
