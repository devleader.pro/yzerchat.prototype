const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_ERROR': {
      return { ...state, loginError: action.payload }
    }

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
