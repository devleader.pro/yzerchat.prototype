import { combineReducers } from 'redux'

import user from './user'
import contacts from './contacts'
import chats from './chats'
import channels from './channels'
import activeChat from './activeChat'
import activeChannel from './activeChannel'
import translates from './translates'
import errors from './errors'

function lastAction(state = null, action) {
  return action.type
}

const rootReducer = combineReducers({
  user,
  contacts,
  chats,
  channels,
  activeChat,
  activeChannel,
  translates,
  errors,
  lastAction,
})

export default rootReducer
