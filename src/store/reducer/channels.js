import news from '../../images/posts/news.png'
import cat from '../../images/posts/cat.jpg'
import hajabusa from '../../images/posts/hajabusa.jpg'
import neuro from '../../images/posts/neuro.jpg'

import logo_1 from '../../images/posts/logo_1.jpg'
import post_1 from '../../images/posts/post_1.jpg'
import post_2 from '../../images/posts/post_2.jpg'
import post_3 from '../../images/posts/post_3.jpg'
import post_4 from '../../images/posts/post_4.jpg'
import post_5 from '../../images/posts/post_5.jpg'
import post_6 from '../../images/posts/post_6.jpg'
import post_7 from '../../images/posts/post_7.jpg'

import logo_2 from '../../images/posts/logo_2.jpg'
import a_post_1 from '../../images/posts/a_post_1.jpg'
import a_post_2 from '../../images/posts/a_post_2.jpg'
import a_post_3 from '../../images/posts/a_post_3.jpg'
import a_post_4 from '../../images/posts/a_post_4.jpg'
import a_post_5 from '../../images/posts/a_post_5.jpg'
import a_post_6 from '../../images/posts/a_post_6.jpg'
const initialState = [
  {
    id: 100,
    name: 'Nature',
    avatar: logo_1,
    members: 2198,
    unreadCount: 0,
    translate: {
      active: true,
      lang: 'ru',
    },
    lastMessage: {
      text: '',
      timestamp: 1569593201623,
    },
    messages: [
      {
        id: 1488,
        img: post_1,
        text: '',

        timestamp: 1569593201623,
        status: 0,
        views: 14968,
        likes: 379,
        reposts: 0,
        liked: false,
      },
      {
        id: 1489,
        img: post_2,
        text: '',

        timestamp: 1569591301623,
        status: 0,
        views: 7432,
        likes: 322,
        reposts: 0,
        liked: false,
      },
      {
        id: 1490,
        img: post_3,
        text: '',
        timestamp: 1569583301623,
        status: 1,
        views: 4123,
        likes: 1230,
        reposts: 330,
        liked: false,
      },
      {
        id: 1491,
        img: post_4,
        text: '',
        timestamp: 1569583201623,
        status: 0,
        views: 22033,
        likes: 1140,
        reposts: 250,
        liked: true,
      },
      {
        id: 1492,
        img: post_5,
        text: '',
        timestamp: 1569583101223,
        status: 0,
        views: 12753,
        likes: 3536,
        reposts: 140,
        liked: false,
      },
      {
        id: 1493,
        img: post_6,
        text: '',
        timestamp: 1569583300023,
        status: 1,
        views: 15210,
        likes: 320,
        reposts: 30,
        liked: true,
      },
      {
        id: 1494,
        img: post_7,
        text: '',
        timestamp: 1568583301623,
        status: 1,
        views: 10719,
        likes: 920,
        reposts: 163,
        liked: true,
      },
    ],
  },
  {
    id: 200,
    name: 'Good News',
    avatar: news,
    members: 1488,
    unreadCount: 0,
    translate: {
      active: true,
      lang: 'ru',
    },
    lastMessage: {
      text: 'Если Вы не можете...',
      timestamp: 1568213138858,
    },
    messages: [
      {
        id: 5465775,
        text:
          'Межпланетная станция «Хаябуса-2» вышла из безопасного режима, в который перешла несколько дней назад при проверке своей системы ориентации. Из-за этого команде миссии пришлось перенести на неопределенный срок операцию по сбросу маркеров-целеуказателей, которые нужны при высадке на поверхность астероида Рюгу последнего оставшегося на борту станции спускаемого модуля MINERVA-II2',
        img: hajabusa,
        timestamp: 12321321321,
        status: 0,
        views: 0,
        likes: 0,
        reposts: 0,
        liked: false,
      },
      {
        id: 1759231,
        text:
          'Алгоритмы компьютерного зрения уже научились отлично решать многие задачи, но проблем у них остается немало. Например, зеркала в кадре часто заставляют алгоритмы определения глубины думать, что отражение в зеркале — это далекие от камеры объекты. Китайские разработчики научили нейросеть почти безошибочно распознавать зеркала в кадре по контрасту между областями снимка с зеркалом и без него',
        img: neuro,
        timestamp: 12321552321,
        status: 0,
        views: 110,
        likes: 20,
        reposts: 0,
        liked: false,
      },
      {
        id: 9889823,
        text:
          'Палеонтологи нашли на японском острове Хоккайдо окаменелости нового рода и вида гадрозавров — травоядных рептилий с утиными клювом. Kamuysaurus japonicus («божественный ящер из Японии»), как назвали новый таксон, жил около 72 миллионов лет назад и был довольно крупной рептилией. Его длина достигала восьми метров, а вес — 4-5 тонн',
        timestamp: 453421332211,
        status: 1,
        views: 2220,
        likes: 1230,
        reposts: 330,
        liked: false,
      },
      {
        id: 12330987,
        text:
          'Астрономы в ходе наблюдений за уникальным саморазрушающимся астероидом 6478 Голт, который в начале этого года обзавелся аж двумя пылевыми хвостами, впервые увидели смену цвета подобного объекта в режиме реального времени. Предполагается, что это вызвано улетом старой пыли с поверхности Голта и обнажением свежего подповерхностного вещества, причем за эти процессы ответственно быстрое вращение астероида, которое возникает из-за YORP-эффекта',
        timestamp: 22321321321,
        status: 0,
        views: 220,
        likes: 110,
        reposts: 20,
        liked: true,
      },
      {
        id: 3321321321,
        text:
          'Цвет оперения сделал сипух более удачливыми охотниками. Птицы рыжего цвета в лунные ночи приносили меньше добычи, чем в новолуние. А белые сипухи приносили в гнездо примерно одинаковое количество еды и в лунные, и в безлунные ночи. Как выяснили ученые, вероятно, дело в лунном свете, который отражается от оперения и ослепляет полевок (основную добычу европейских сипух)',
        timestamp: 32321552321,
        status: 0,
        views: 0,
        likes: 0,
        reposts: 0,
        liked: false,
      },
      {
        id: 88721321,
        text:
          'Если вы не можете спать из-за шума проезжающих мимо вашего дома машин и мотоциклов, то можете переехать во Францию: в одном из парижских пригородов начали тестировать систему, которая регистрирует шум от транспортных средств. В будущем ее подключат к камерам наблюдения, после чего тем, кто сильно шумит, будут выписывать штрафы',
        timestamp: 453421332211,
        status: 1,
        views: 1110,
        likes: 20,
        reposts: 10,
        liked: true,
      },
      {
        id: 88721322,
        img: cat,
        text: '',
        timestamp: 453421332212,
        status: 1,
        views: 10719,
        likes: 920,
        reposts: 163,
        liked: true,
      },
    ],
  },
]

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_CHANNEL':
      return [...state, action.payload]

    case 'RESET_STATE': {
      return initialState
    }

    default:
      return state
  }
}
