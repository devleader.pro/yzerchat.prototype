import { createAction } from 'redux-actions'

import findLastIndex from 'lodash/findLastIndex'

import MatrixClient from '../services/MatrixClient'
import Translator from '../network/translate'

import {
  readLocalStorage,
  clearLocalStorage,
  getRoomSettings,
  setRoomSettings,
} from '../services/utils'

export const loginRequest = createAction('LOGIN_REQUEST')
export const loginSuccess = createAction('LOGIN_SUCCESS')
export const loginFail = createAction('LOGIN_FAIL')

export const loginConfirmRequest = createAction('LOGIN_CONFIRM_REQUEST')
export const loginConfirmSuccess = createAction('LOGIN_CONFIRM_SUCCESS')
export const loginConfirmFail = createAction('LOGIN_CONFIRM_FAIL')

export const logoutRequest = createAction('LOGOUT_REQUEST')
export const logoutSuccess = createAction('LOGOUT_SUCCESS')
export const logoutFail = createAction('LOGOUT_FAIL')

export const setIsLoggedIn = createAction('SET_IS_LOGGED_IN')

export function init() {
  return dispatch => {
    const { accessToken, userId, deviceId } = readLocalStorage([
      'accessToken',
      'userId',
      'deviceId',
    ])
    if (accessToken && userId && deviceId) {
      // try login with token

      MatrixClient._restoreSessionFromLocalStorage(
        accessToken,
        userId,
        deviceId,
      )

      dispatch(start())
    } else {
      // show login form
      dispatch(setIsLoggedIn(false))
    }
  }
}

export function prepareData() {
  return (dispatch, getState) => {
    const client = MatrixClient.get()
    const userId = client.getUserId()

    client.getProfileInfo(userId).then(info => {
      dispatch({
        type: 'UPDATE_PROFILE',
        payload: {
          id: userId,
          name: info.displayname,
          avatar: client.mxcUrlToHttp(info.avatar_url),
        },
      })
    })

    const rooms = client.getRooms().filter(room => {
      return room._selfMembership === 'join'
    })

    let members = {}

    rooms
      .filter(room => {
        return Object.keys(room.currentState.members).length === 2
      })
      .forEach(room => {
        return (members = { ...members, ...room.currentState.members })
      })

    delete members[client.getUserId()]

    const contactsId = Object.keys(members)

    const contacts = contactsId.map(userId => {
      const userProfile = client.store.users[userId]

      return {
        id: userId,
        name: userProfile.displayName,
        avatar: client.mxcUrlToHttp(userProfile.avatarUrl),
        online: userProfile.presence === 'online',
        lastSeen: userProfile.lastPresenceTs,
        roomId: members[userId].roomId,
      }
    })

    const chats = rooms.map(room => {
      const lastMsgIndex = findLastIndex(room.timeline, function(item) {
        return item.event.type === 'm.room.message'
      })

      const lastReadEventId =
        room.accountData['m.fully_read'].event.content.event_id
      const lastReadEventIndex = room.timeline.findIndex(event => {
        return event.event.event_id === lastReadEventId
      })

      const unreadMessagesCount = room.timeline
        .slice(lastReadEventIndex + 1)
        .filter(event => {
          return event.event.type === 'm.room.message'
        }).length

      const avatar =
        contacts.find(contact => {
          return contact.roomId === room.roomId
        })?.avatar || ''

      const chatData = {
        id: room.roomId,
        name: room.name,
        avatar: avatar,
        lastMessage: null,
        translate: getRoomSettings(room.roomId),
        unreadMessagesCount,
      }

      if (lastMsgIndex !== -1) {
        const lastMsg = room.timeline[lastMsgIndex].event
        const anyMember = room
          .getJoinedMembers()
          .filter(({ userId }) => userId !== client.getUserId())?.[0]

        const status =
          anyMember && room.hasUserReadEvent(anyMember.userId, lastMsg.event_id)
            ? 1
            : 0

        chatData.lastMessage = {
          id: lastMsg.event_id,
          text: lastMsg.content.body,
          timestamp: lastMsg.origin_server_ts,
          status: status,
          type: [lastMsg.sender, lastMsg.user_id].includes(userId) ? 0 : 1,
        }
      }
      return chatData
    })

    dispatch({
      type: 'SYNC_CHATS',
      payload: chats,
    })

    dispatch({
      type: 'SYNC_CONTACTS',
      payload: contacts,
    })

    client.on('Room.timeline', function(
      event,
      room,
      toStartOfTimeline,
      removed,
      data,
    ) {
      const { activeChat } = getState()
      const activeChatId = activeChat.id
      const activeChatOpened = activeChat.opened

      if (event.getType() === 'm.room.message' && data.liveEvent) {
        dispatch(
          updateLastMessage(
            room.roomId,
            event.event,
            !activeChatOpened && event.sender.userId !== userId,
          ),
        )
        if (activeChatId === room.roomId && activeChatId) {
          dispatch(updateActiveChatMessages(room.roomId))
        }
      }
    })
    dispatch({ type: 'PREPARE_DATA' })

    client.on('RoomMember.typing', function(event, user) {
      dispatch(setUserTyping(user))
    })

    client.on('Room.receipt', function(event, room) {
      const content = event.getContent()
      const isSelf =
        Object.keys(content).filter(eid => {
          return Object.keys(content[eid]['m.read']).includes(
            client.getUserId(),
          )
        }).length > 0

      if (isSelf) return

      const roomId = room.roomId
      const eventId = Object.keys(content)[0]
      dispatch(setActiveChatMessageStatus(roomId, eventId))
    })
  }
}

export function setError(errorType, bool) {
  return {
    type: errorType,
    payload: bool,
  }
}

export function attemptPasswordLogin(user, password) {
  return dispatch => {
    MatrixClient.loginWithPassword(user, password)
      .then(response => {
        const { access_token, user_id, device_id } = response

        localStorage.setItem('accessToken', access_token)
        localStorage.setItem('userId', user_id)
        localStorage.setItem('deviceId', device_id)
        dispatch(start())
      })
      .catch(err => {
        dispatch(setError('LOGIN_ERROR', true))
        dispatch(setIsLoggedIn(false))
        // setTimeout(() => {
        //   dispatch(setError('LOGIN_ERROR', false))
        // }, 3000)
        clearLocalStorage()
        console.log('attemptPasswordLogin Error: ', err)
      })
  }
}

export function start() {
  return (dispatch, getState) => {
    MatrixClient.get().once('sync', syncState => {
      console.log('*** FIRST SYNC: ', syncState)
      dispatch(firstSync({ status: syncState }))
      dispatch(prepareData())
    })

    MatrixClient.start()
      .then(() => {
        dispatch(setIsLoggedIn(true))
      })
      .catch(err => {
        console.log('restoreError: ', err)
        dispatch(setIsLoggedIn(false))
      })
  }
}

export function logout() {
  return dispatch => {
    MatrixClient.logout()
    clearLocalStorage()
    dispatch({ type: 'RESET_STATE' })
    dispatch(logoutSuccess())
  }
}

export function updateLastMessage(roomId, event, incCount = false) {
  return dispatch => {
    const client = MatrixClient.get()
    const myUserId = client.getUserId()
    const room = client.getRoom(roomId)
    const anyMember = room
      .getJoinedMembers()
      .filter(({ userId }) => userId !== myUserId)?.[0]

    const status =
      anyMember && room.hasUserReadEvent(anyMember.userId, event.event_id)
        ? 1
        : 0

    const lastMessage = {
      id: event.event_id,
      status: status,
      text: event.content.body,
      timestamp: event.origin_server_ts,
      type: [event.sender, event.user_id].includes(myUserId) ? 0 : 1,
    }
    dispatch(
      checkNeedTranslate(
        [
          {
            id: lastMessage.id,
            text: lastMessage.text,
            type: lastMessage.type,
          },
        ],
        roomId,
      ),
    )
    dispatch({
      type: 'UPDATE_LAST_MESSAGE',
      payload: { roomId, lastMessage, incCount },
    })
  }
}

export function acceptPrivacy() {
  localStorage.setItem('privacyAccepted', true)
  return {
    type: 'ACCEPT_PRIVACY',
    payload: true,
  }
}

export function firstSync(payload) {
  return {
    type: 'FIRST_SYNC',
    status: payload.status,
  }
}

export function setActiveChat(id) {
  return (dispatch, getState) => {
    const { activeChat } = getState()
    const { messages, paginationToken } = MatrixClient.loadRoomMessages(
      id,
      activeChat.translatedManually,
    )
    //const translate = getRoomSettings(id)
    dispatch(checkNeedTranslate(messages, id))

    dispatch({
      type: 'SET_ACTIVE_CHAT',
      payload: {
        id,
        messages,
        paginationToken,
        //translate,
      },
    })
    if (messages.length) {
      const lastMsgId = messages[messages.length - 1].id
      MatrixClient.setReadMarkers(id, lastMsgId).then(() => {
        dispatch(updateRoomMessagesUnreadCount(id, 0))
      })
    }
  }
}

export function setActiveChannel(id) {
  return { type: 'SET_ACTIVE_CHANNEL', id }
}

export function closeActiveChat() {
  return {
    type: 'CLOSE_ACTIVE_CHAT',
  }
}

export function updateRoomMessagesUnreadCount(roomId, count) {
  return {
    type: 'UPDATE_UNREAD_ROOM_MESSAGES_COUNT',
    payload: {
      roomId,
      count,
    },
  }
}

export function updateActiveChatMessages(roomId) {
  return (dispatch, getState) => {
    const { activeChat } = getState()
    const { messages, paginationToken } = MatrixClient.loadRoomMessages(
      roomId,
      activeChat.translatedManually,
    )
    dispatch(checkNeedTranslate(messages, roomId))

    dispatch({
      type: 'UPDATE_ACTIVE_CHAT',
      payload: {
        messages,
        paginationToken,
      },
    })

    if (activeChat.opened) {
      if (messages.length) {
        const lastMsgId = messages[messages.length - 1].id
        MatrixClient.setReadMarkers(roomId, lastMsgId).then(() => {
          dispatch(updateRoomMessagesUnreadCount(roomId, 0))
        })
      }
    }
  }
}

export function setActiveChatMessageStatus(roomId, msgId) {
  return (dispatch, getState) => {
    const { activeChat } = getState()

    dispatch({
      type: 'SET_CHAT_LAST_MESSAGE_STATUS',
      payload: {
        msgId,
        roomId,
        status: 1,
      },
    })

    if (activeChat.opened && activeChat.id === roomId) {
      dispatch({
        type: 'SET_ACTIVE_CHAT_MESSAGE_STATUS',
        payload: {
          msgId,
          status: 1,
        },
      })
    }
  }
}

export function toggleMessageShowTranslate(msgId) {
  return (dispatch, getState) => {
    const { activeChat, chats, translates } = getState()

    const lang = chats.find(chat => {
      return chat.id === activeChat.id
    }).translate.lang

    const message = activeChat.messages.find(msg => {
      return msg.id === msgId
    })

    const showTranslate = !message.showTranslate

    if (showTranslate) {
      if (translates[lang]?.[msgId]) {
      } else {
        dispatch(translateText(msgId, message.text, lang))
      }
    }

    dispatch({
      type: 'TOGGLE_MESSAGE_SHOW_TRANSLATE',
      payload: {
        msgId,
        showTranslate,
      },
    })
  }
}

function checkNeedTranslate(messages, roomId) {
  return (dispatch, getState) => {
    const { lang, active } = getRoomSettings(roomId)
    const { translates } = getState()
    if (active) {
      messages.forEach(msg => {
        if (!translates[lang]?.[msg.id] && msg.type !== 0) {
          dispatch(translateText(msg.id, msg.text, lang))
        }
      })
    }
  }
}

export function translateText(msgId, text, lang) {
  return dispatch => {
    Translator.translate(text, lang)
      .then(translatedText => {
        dispatch({
          type: 'TRANSLATE_TEXT',
          payload: { msgId, lang, translatedText },
        })
      })
      .catch(err => {
        console.log('translate error: ', err)
      })
  }
}

export function setChatTranslateSettings(id, lang = 'en', active = false) {
  return dispatch => {
    setRoomSettings(id, { lang, active })

    dispatch({
      type: 'SET_CHAT_TRANSLATE_SETTINGS',
      payload: {
        id,
        translate: {
          active,
          lang,
        },
      },
    })
    dispatch(updateActiveChatMessages(id))
  }
}

export function scrollbackRoom(roomId) {
  return dispatch => {
    dispatch({ type: 'LOADING_HISTORY', payload: true })
    const client = MatrixClient.get()
    client.scrollback(client.getRoom(roomId), 30).then(() => {
      dispatch({ type: 'LOADING_HISTORY', payload: false })
      dispatch(updateActiveChatMessages(roomId))
    })
  }
}

export function setUserTyping(user) {
  return dispatch => {
    if (MatrixClient.get().getUserId() !== user.userId) {
      const { roomId, typing, name } = user
      dispatch({ type: 'USER_TYPING', payload: { roomId, typing, name } })
    }
  }
}

export function setDisplayName(name) {
  return dispatch => {
    MatrixClient.get()
      .setDisplayName(name)
      .then(data => {
        dispatch({
          type: 'UPDATE_PROFILE',
          payload: {
            name,
          },
        })
      })
  }
}

// export function sendTyping () {
//   MatrixClient.get().sendTyping()
// }
