import Matrix from 'matrix-js-sdk'

import _ from 'lodash'

import { getRoomSettings } from './utils'

import { format } from 'date-fns'

/**
  Обертка над Matrix Client из matrix-js-sdk
  Singleton (один экземпляр на все приложение)
*/
const config = {
  baseUrl: 'https://chat.devlead.pro',
}

class MatrixClient {
  constructor() {
    this.matrixClient = null

    this.opts = {
      initialSyncLimit: 30,
      timelineSupport: true,
      pendingEventOrdering: 'detached',
    }
  }

  get() {
    return this.matrixClient
  }

  unset() {
    this.matrixClient = null
  }

  updateOpts() {}

  _createClient(accessToken, userId, deviceId) {
    this.matrixClient = Matrix.createClient({
      baseUrl: config.baseUrl,
      accessToken,
      userId,
      deviceId,
      ...this.opts,
    })
  }

  addListeners() {
    // subscribe on events
  }

  _restoreSessionFromLocalStorage(accessToken, userId, deviceId) {
    this._createClient(accessToken, userId, deviceId)
  }

  async loginWithToken(accessToken) {
    const tempClient = Matrix.createClient(config.baseUrl)
    const data = await tempClient.loginWithToken(accessToken)
    return data
  }

  async loginWithPassword(user, password) {
    this.matrixClient = Matrix.createClient(config.baseUrl)
    const data = await this.get().loginWithPassword(user, password)
    return data
  }

  async start() {
    console.log('*** start MatrixClient ***')

    const client = this.get()

    await client.startClient(this.opts)
    console.log('*** MatrixClient started ***')
  }

  logout() {
    this.get().stopClient()
    this.get().logout()
    this.unset()
  }

  async sendTextMessage(roomId, text) {
    const content = { body: text, msgtype: 'm.text' }
    this.get().sendEvent(roomId, 'm.room.message', content, '')
  }

  loadRoomMessages(roomId, manually) {
    const client = this.get()
    const room = client.getRoom(roomId)
    const paginationToken = room.oldState.paginationToken
    const roomTimeline = room.timeline
    const anyMember = room
      .getJoinedMembers()
      .filter(({ userId }) => userId !== client.getUserId())[0]
    const messages = roomTimeline
      .filter(event => {
        return event.event.type === 'm.room.message'
      })
      .map(event => {
        const msg = event.event
        const status = room.hasUserReadEvent(anyMember.userId, msg.event_id)
          ? 1
          : 0
        return {
          id: msg.event_id,
          text: msg.content.body,
          timestamp: msg.origin_server_ts,
          status: status,
          type: [msg.sender, msg.user_id].includes(client.getUserId()) ? 0 : 1,
          date: format(msg.origin_server_ts, 'dd.MM.yy'),
          showTranslate:
            manually[msg.event_id] !== undefined
              ? manually[msg.event_id]
              : getRoomSettings(roomId).active,
        }
      })
    return { messages, paginationToken }
  }

  async setReadMarkers(roomId, eventId) {
    const client = this.get()
    const event = client.getRoom(roomId).findEventById(eventId)
    await client.setRoomReadMarkers(roomId, eventId)
    await client.sendReadReceipt(event)
  }
}

if (!window.mxMatrixClient) {
  window.mxMatrixClient = new MatrixClient()
}
export default window.mxMatrixClient
