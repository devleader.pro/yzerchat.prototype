export function getTokenFromLocalStorage() {
  return localStorage.getItem('accessToken')
}

export function clearLocalStorage() {
  const lsItems = ['userId', 'accessToken', 'deviceId']
  lsItems.forEach(item => {
    localStorage.removeItem(item)
  })
}

export function readLocalStorage(keys) {
  const result = {}

  keys.forEach(key => {
    result[key] = localStorage.getItem(key)
  })
  return result
}

export const setRoomSettings = (roomId, settingsObject) => {
  localStorage.setItem(roomId, JSON.stringify(settingsObject))
}

export const getRoomSettings = roomId => {
  const defaultTranslateSettings = {
    active: false,
    lang: 'en',
  }
  return JSON.parse(localStorage.getItem(roomId)) || defaultTranslateSettings
}

// export const selectTranslate = (msgId, lang) => {

// }
