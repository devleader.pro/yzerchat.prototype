import React, { Component } from 'react'
import { connect } from 'react-redux'

import { init } from './store/actions'

import { Navigator } from 'react-onsenui'

import RootTabs from './components/RootTabs'
import LoginWithPasswordPage from './pages/LoginWithPasswordPage'
import LoginPage from './pages/LoginPage'
import PrivacyPage from './pages/PrivacyPage'
import logo from './images/logo.png'

const renderPage = (route, navigator) => {
  route.props = route.props || {}
  route.props.navigator = navigator

  return React.createElement(route.component, route.props)
}

class Root extends Component {
  componentDidMount() {
    this.props.init()
  }

  render() {
    const { isLoggedIn, privacyAccepted } = this.props
    if (!privacyAccepted) {
      return (
        <Navigator
          key="PrivacyPage"
          initialRoute={{
            component: PrivacyPage,
            props: { key: 'PrivacyPage' },
          }}
          renderPage={renderPage}
        />
      )
    }
    if (isLoggedIn === null) {
      return (
        <div className="start-screen">
          <img className="start-screen__logo" src={logo} />
        </div>
      )
    }

    if (isLoggedIn === false) {
      return (
        <Navigator
          key="LoginPage"
          initialRoute={{
            component: LoginPage,
            props: { key: 'LoginPage' },
          }}
          renderPage={renderPage}
          animation="slide"
        />
      )
    }

    if (isLoggedIn === true) {
      return (
        <Navigator
          key="RootTabs"
          initialRoute={{
            component: RootTabs,
            props: {
              key: 'RootTabs',
            },
          }}
          renderPage={renderPage}
        />
      )
    }

    return null
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn,
  privacyAccepted: state.user.privacyAccepted,
})

export default connect(
  mapStateToProps,
  { init },
)(Root)
