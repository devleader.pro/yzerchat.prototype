import 'react-hot-loader'
import '@babel/polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import configureStore from './store/configure'

import App from './App'

import ons from 'onsenui'
import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'

import 'onsenui/css/ionicons/css/ionicons.min.css'
import 'onsenui/css/material-design-iconic-font/css/material-design-iconic-font.min.css'

import './styles/override.css'

import './styles/main.scss'

const store = configureStore({})

store.subscribe(() => {
  const { translates, lastAction } = store.getState()
  if (lastAction === 'TRANSLATE_TEXT') {
    localStorage.setItem('yzch_translates', JSON.stringify(translates))
  }
})

const renderApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

const root = document.getElementById('app')

window.addEventListener('resize', function() {
  if (document.querySelector('ons-navigator')) {
    document.querySelector('ons-navigator')._show()
  }
})

ons.ready(() => {
  render(renderApp(), root)
})
