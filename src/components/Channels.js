import React, { useState } from 'react'
import { List, ListItem, Fab, Icon, Popover } from 'react-onsenui'
import Avatar from 'react-avatar'
import { getLastMsgDateTime } from '../utils'

const Channels = ({ openChannel, channels, newChat }) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)
  let refs = null

  return (
    <>
      <List
        dataSource={channels}
        renderRow={(channel, idx) => (
          <ListItem
            style={{ width: '100%' }}
            tappable={true}
            modifier="material"
            onClick={() => openChannel(channel.id, channel.name, channel)}
            key={channel.name}
          >
            <div className="left">
              {channel.avatar ? (
                <img className="list-item__thumbnail" src={channel.avatar} />
              ) : (
                <Avatar
                  name={channel.name}
                  size={40}
                  round="20px"
                  color={Avatar.getRandomColor('sitebase', [
                    'red',
                    'green',
                    'blue',
                  ])}
                />
              )}
            </div>
            <div className="center">
              <span className="list-item__title">{channel.name}</span>
              {channel.lastMessage.text ? (
                <span className="list-item__subtitle">
                  {channel.lastMessage.text}
                </span>
              ) : (
                <span className="list-item__subtitle">sad</span>
              )}
              <span className="list-item__subtitle">
                {channel.members} подписчиков
              </span>
            </div>

            <div className="right" style={{ alignItems: 'flex-start' }}>
              <div className="last-msg-time">
                {getLastMsgDateTime(channel.lastMessage.timestamp)}
              </div>
            </div>
          </ListItem>
        )}
      />

      <Fab
        position="bottom right"
        ref={btn => {
          refs = btn
        }}
        onClick={() => setMenuIsOpen(true)}
      >
        <Icon icon="ion-md-add" style={{ verticalAlign: 'middle' }}></Icon>
      </Fab>

      <Popover
        isOpen={menuIsOpen}
        onCancel={() => setMenuIsOpen(false)}
        getTarget={() => refs}
        maskColor="#71717159"
      >
        <List>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="ion-ios-megaphone, material:ion-md-megaphone"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div className="center strongblue">Новый канал</div>
          </ListItem>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="ion-md-people"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div className="center strongblue">Новая группа</div>
          </ListItem>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="md-comment-text-alt"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div
              className="center strongblue"
              onClick={() => {
                setMenuIsOpen(false)
                newChat()
              }}
            >
              Новый чат
            </div>
          </ListItem>
        </List>
      </Popover>
    </>
  )
}

export default Channels
