import React from 'react'
import { Page, Toolbar, BackButton, Icon } from 'react-onsenui'

export default class PrivacyPolicy extends React.Component {
  render() {
    return (
      <Page className="policy-page">
        <Toolbar>
          <div className="left">
            <BackButton
              onClick={() => {
                this.props.navigator.popPage()
              }}
            >
              Назад
            </BackButton>
          </div>
        </Toolbar>
        <div className="policy">
          <div>
            Privacy Policy of YZER
            <br /> YZER collects some Personal Data from its Users.
          </div>
          <div className="summary">Policy summary</div>
          <h4>
            Personal Data collected for the following purposes and using the
            following services:
          </h4>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-stats" className="item-icon" />
              </div>

              <div className="text">Analytics</div>
            </div>
            <div className="item-text">
              Google Analytics, Appsflyer and Amplitude Personal Data: Cookies;
              Usage Data
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-md-mail-open" className="item-icon" />
              </div>

              <div className="text">Contacting the User</div>
            </div>
            <div className="item-text">
              Contact form Personal Data:
              <br /> email address; first name; phone number; User ID; various
              types of Data <br />
              <br /> Phone contact Personal Data: <br /> phone number
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-text" className="item-icon" />
              </div>

              <div className="text">Content commenting</div>
            </div>
            <div className="item-text">
              Comment system managed directly Personal Data:
              <br /> first name; last name; Usage Data; username
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Data transfer outside the EU</div>
            </div>
            <div className="item-text">
              Data transfer abroad based on standard contractual clauses and
              Other legal basis for Data transfer abroad
              <br />
              <br />
              Personal Data:
              <br /> various types of Data
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">
                Device permissions for Personal Data access
              </div>
            </div>
            <div className="item-text">
              Device permissions for Personal Data access Personal Data: <br />
              <br />
              Approximate location permission (non-continuous); Camera
              permission; Contacts permission; Microphone permission; Photo
              Library permission; Precise location permission (non-continuous);
              Storage permission
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Hosting and backend infrastructure</div>
            </div>
            <div className="item-text">
              Firebase Cloud Functions
              <br /> <br />
              Personal Data:
              <br />
              Usage Data; various types of Data as specified in the privacy
              policy of the service
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Infrastructure monitoring</div>
            </div>
            <div className="item-text">
              Crashlytics Personal Data:
              <br /> geographic position; unique device identifiers for
              advertising (Google Advertiser ID or IDFA, for example); various
              types of Data as specified in the privacy policy of the service
              <br /> <br />
              Firebase Performance Monitoring <br /> <br />
              Personal Data: various types of Data as specified in the privacy
              policy of the service
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">
                Interaction with support and feedback platforms
              </div>
            </div>
            <div className="item-text">
              Zendesk Widget
              <br />
              Personal Data: email address; first name; phone number
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Location-based interactions</div>
            </div>
            <div className="item-text">
              Non-continuous geolocation <br />
              Personal Data: geographic position
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Managing support and contact requests</div>
            </div>
            <div className="item-text">
              Zendesk
              <br />
              Personal Data: various types of Data as specified in the privacy
              policy of the service
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Social features</div>
            </div>
            <div className="item-text">
              Inviting and suggesting friends
              <br />
              Personal Data: various types of Data
              <br />
              <br />
              Public profile Personal Data: username
            </div>
          </div>
          <h4>Further information about Personal Data</h4>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Push notifications</div>
            </div>
            <div className="item-text">
              YZER may send push notifications to the User.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Unique device identification</div>
            </div>
            <div className="item-text">
              YZER may track Users by storing a unique identifier of their
              device, for analytics purposes or for storing Users' preferences.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Access the address book</div>
            </div>
            <div className="item-text">
              YZER may request access to your address book.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">
                Personal Data collected through sources other than the User
              </div>
            </div>
            <div className="item-text">
              The Owner of YZER may have legitimately collected Personal Data
              relating to Users without their knowledge by reusing or sourcing
              them from third parties on the grounds mentioned in the section
              specifying the legal basis of processing. Where the Owner has
              collected Personal Data in such a manner, Users may find specific
              information regarding the source within the relevant sections of
              this document or by contacting the Owner.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">Pseudonymous use</div>
            </div>
            <div className="item-text">
              When registering for YZER, Users have the option to indicate a
              nickname or pseudonym. In this case, Users' Personal Data shall
              not be published or made publicly available. Any activity
              performed by Users on YZER shall appear in connection with the
              indicated nickname or pseudonym. However, Users acknowledge and
              accept that their activity on YZER, including content, information
              or any other material possibly uploaded or shared on a voluntary
              and intentional basis may directly or indirectly reveal their
              identity.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">
                The Service is not directed to children under the age of 13
              </div>
            </div>
            <div className="item-text">
              Users declare themselves to be adult according to their applicable
              legislation. Minors may use YZER only with the assistance of a
              parent or guardian. Under no circumstance persons under the age of
              13 may use YZER.
            </div>
          </div>
          <div className="item">
            <div className="item-title">
              <div className="icon-container">
                {' '}
                <Icon icon="ion-ios-document" className="item-icon" />
              </div>

              <div className="text">
                User identification via a universally unique identifier (UUID)
              </div>
            </div>
            <div className="item-text">
              YZER may track Users by storing a so-called universally unique
              identifier (or short UUID) for analytics purposes or for storing
              Users' preferences. This identifier is generated upon installation
              of this Application, it persists between Application launches and
              updates, but it is lost when the User deletes the Application. A
              reinstall generates a new UUID
            </div>
          </div>
        </div>
      </Page>
    )
  }
}
