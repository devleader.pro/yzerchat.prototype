import React, { Fragment } from 'react'

import { format, isToday, isYesterday } from 'date-fns'
import { ru } from 'date-fns/locale'

import { Button, Icon } from 'react-onsenui'

const getDay = timestamp => {
  if (isYesterday(timestamp)) {
    return 'Вчера'
  }
  if (isToday(timestamp)) {
    return 'Сегодня'
  }
  return format(timestamp, 'dd MMMM', { locale: ru })
}

const Channel = ({ activeChannel, message, handleChange, handleSubmit }) => {
  const { id, name, messages } = activeChannel

  const sortedMessages = [...messages].sort((a, b) => {
    return a.timestamp - b.timestamp
  })

  let currentDate

  return (
    <div className="chat">
      <ul className="chat__messages-list">
        {sortedMessages.map(msg => {
          const msgView = (
            <li className={`post-li`} key={msg.id}>
              <div className="post">
                <div className="post-header">
                  <div>
                    <Icon
                      icon="ion-md-eye"
                      size={18}
                      style={{
                        color: '#CCCCCC',
                        paddingBottom: '3px',
                        verticalAlign: 'middle',
                      }}
                    />
                    <span> {msg.views}</span>
                  </div>
                  <div>
                    <span className="post-header-time">
                      {format(msg.timestamp, 'hh:mm')}
                    </span>
                  </div>
                </div>
                {msg.img && <img src={msg.img} style={{ width: '100%' }} />}
                {msg.text && <div className="post-body">{msg.text}</div>}

                <div className="post-footer">
                  <Button
                    className="post post-button post-button-left"
                    modifier="light"
                  >
                    <Icon
                      icon="ion-md-heart"
                      size={18}
                      style={{
                        color: msg.liked ? '#4098E0' : '#CCCCCC',
                        paddingBottom: '3px',
                      }}
                    />
                    <span>{msg.likes}</span>
                  </Button>
                  <Button
                    className="post post-button post-button-right"
                    modifier="light"
                  >
                    <Icon
                      icon="ion-md-share-alt"
                      size={25}
                      style={{ color: '#CCCCCC', paddingBottom: '3px' }}
                    />{' '}
                    <span>{msg.reposts}</span>
                  </Button>
                </div>
              </div>
            </li>
          )

          const formattedDate = format(msg.timestamp, 'dd.MM.yyyy')

          if (currentDate !== formattedDate) {
            currentDate = formattedDate
            return (
              <Fragment key={formattedDate}>
                <li
                  key={formattedDate}
                  className="chat__messages-day"
                  style={{ margin: '15px 0' }}
                >
                  {getDay(msg.timestamp)}
                </li>
                {msgView}
              </Fragment>
            )
          }
          return msgView
        })}
      </ul>
    </div>
  )
}

export default Channel
