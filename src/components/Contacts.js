import React from 'react'
import { List, ListItem, ListHeader, SpeedDial, Fab, Icon } from 'react-onsenui'
import { lastSeenFormat } from '../utils'
import Avatar from 'react-avatar'
import ic_contacts_empty from '../images/ic_contacts_empty.png'
const Contacts = ({ openChat, contacts }) => {
  if (!contacts.length) {
    return (
      <div className="contacts-empty">
        <img src={ic_contacts_empty} className="contacts-empty-img" />
        <p>
          Список контактов пуст...
          <br />
          Приглашайте друзей и начинайте общение
        </p>
      </div>
    )
  }

  const getPresence = user => {
    if (user.online) {
      return <span className="list-item__subtitle strongblue">в сети</span>
    }

    if (user.lastSeen === 0) {
      return <span className="list-item__subtitle">не в сети</span>
    }

    return (
      <span className="list-item__subtitle">
        был в сети {lastSeenFormat(user.lastSeen)}
      </span>
    )
  }

  return (
    <>
      <List>
        <ListItem tappable={true} modifier="material">
          <div className="left strongblue">
            <Icon
              icon="ion-md-people"
              size={26}
              fixedWidth={false}
              style={{ verticalAlign: 'middle' }}
            />
          </div>
          <div className="center strongblue">Добавить друзей</div>
        </ListItem>
      </List>
      <List
        dataSource={contacts}
        renderHeader={() => (
          <ListHeader
            style={{
              fontSize: 15,
              backgroundColor: '#ECECEC',
              textTransform: 'uppercase',
            }}
            className="testClass"
          >
            Контакты
          </ListHeader>
        )}
        renderRow={(user, idx) => (
          <ListItem
            style={{ width: '100%' }}
            tappable={true}
            modifier="material"
            onClick={() => openChat(user.roomId, name)}
            key={idx}
          >
            <div className="left">
              {user.avatar ? (
                <img className="list-item__thumbnail" src={user.avatar} />
              ) : (
                <Avatar
                  name={user.name}
                  size={40}
                  round="20px"
                  color={Avatar.getRandomColor(user.name, [
                    '#3498db',
                    '#9b59b6',
                    '#e74c3c',
                    '#f1c40f',
                    '#2c3e50',
                    '#d35400',
                    '#1e3799',
                    '#b71540',
                  ])}
                />
              )}
            </div>
            <div className="center">
              <span className="list-item__title">{user.name}</span>
              {getPresence(user)}
            </div>
          </ListItem>
        )}
      />
    </>
  )
}

export default Contacts
