import React, { Component, Fragment } from 'react'

import { format, isToday, isYesterday } from 'date-fns'
import { ru } from 'date-fns/locale'

import { Icon, ProgressBar } from 'react-onsenui'

const msgTypes = ['sent', 'received']
const msgStatuses = ['unread', 'read']

class Chat extends Component {
  state = {
    scrolled: false,
  }

  getMsgText = msg => {
    if (msgTypes[msg.type] === 'sent' || !msg.showTranslate) {
      return msg.text
    }
    return (
      this.props.translates[this.props.chat.translate.lang]?.[msg.id] ||
      msg.text
    )
  }

  getDay = timestamp => {
    if (isYesterday(timestamp)) {
      return 'Вчера'
    }
    if (isToday(timestamp)) {
      return 'Сегодня'
    }
    return format(timestamp, 'dd MMMM', { locale: ru })
  }

  handleScroll = () => {
    this.setState({ scrollPos: this.messagesList.scrollTop })
    if (this.messagesList.scrollTop === 0) {
      if (!this.props.activeChat.loading) {
        this.props.loadHistory()
      }
    }
  }

  componentDidMount() {
    setTimeout(() => {
      if (
        this.props.activeChat?.id === this.props.chat?.id &&
        this.state.scrolled === false &&
        this.messagesList
      ) {
        this.setState({ scrollHeight: this.messagesList.scrollHeight })
        this.messagesList.scroll(0, 10000)
        this.setState({ scrolled: true })

        this.messagesList.addEventListener('scroll', this.handleScroll)
      }
    }, 0)
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.activeChat?.id === this.props.chat?.id &&
      this.state.scrolled === false &&
      this.messagesList &&
      prevProps?.activeChat?.id !== this.props.activeChat?.id
    ) {
      this.setState({ scrollHeight: this.messagesList.scrollHeight })
      this.messagesList.scroll(0, 10000)
      this.setState({ scrolled: true })

      this.messagesList.addEventListener('scroll', this.handleScroll)
    }

    if (
      prevProps.activeChat?.messages.length <
      this.props.activeChat?.messages.length
    ) {
      if (this.state.scrollPos < 10) {
        const scrollHeightDiff =
          this.messagesList.scrollHeight - this.state.scrollHeight

        if (scrollHeightDiff > 0) {
          if (scrollHeightDiff < this.messagesList.offsetHeight) {
            this.messagesList.scroll(0, 1)
          } else {
            this.messagesList.scroll(
              0,
              scrollHeightDiff + 10 - this.messagesList.offsetHeight / 2,
            )
          }
          this.setState({ scrollHeight: this.messagesList.scrollHeight })
        }

        return
      }

      if (
        this.messagesList.scrollHeight -
          this.messagesList.scrollTop -
          this.messagesList.offsetHeight <
        300
      ) {
        this.messagesList.scroll(0, 10000)
      }
    }
  }

  handleFocus = () => {
    if (
      this.messagesList.scrollHeight -
        this.messagesList.scrollTop -
        this.messagesList.offsetHeight <
      300
    ) {
      setTimeout(() => {
        this.messagesList.scroll(0, 10000)
      }, 300)
    }
  }

  onSubmit = e => {
    e.preventDefault()
    this._input.focus()
    this.props.handleSubmit(e)
  }

  render() {
    const {
      activeChat,
      message,
      handleChange,
      handleSubmit,
      toggleMessageShowTranslate,
    } = this.props

    //if (!activeChat) return <div className="chat">Загрузка...</div>

    let currentDate

    return (
      <div className="chat">
        {this.props.activeChat?.loading && <ProgressBar indeterminate />}
        {!!activeChat && (
          <ul
            className="chat__messages-list"
            ref={el => {
              this.messagesList = el
            }}
          >
            {activeChat.messages.map(msg => {
              const msgView = (
                <li
                  className={`chat-message chat-message--${msgTypes[msg.type]}`}
                  key={msg.id}
                >
                  {msgTypes[msg.type] === 'received' ? (
                    <div
                      className={`translate-msg ${
                        msg.showTranslate ? 'rotate' : ''
                      }`}
                      onClick={() => toggleMessageShowTranslate(msg.id)}
                    >
                      <Icon
                        icon="ion-md-swap"
                        style={{
                          margin: 'auto',
                        }}
                      />
                    </div>
                  ) : null}
                  <span className="chat-message__body">
                    {this.getMsgText(msg)}
                  </span>
                  <span className="chat-message__meta">
                    <span className="chat-message__time">
                      {format(msg.timestamp, 'hh:mm')}
                    </span>

                    {msg.type === 0 && (
                      <span
                        className={`chat-message__status chat-message__status--${
                          msgStatuses[msg.status]
                        }`}
                      ></span>
                    )}
                  </span>
                </li>
              )

              if (currentDate !== msg.date) {
                currentDate = msg.date
                return (
                  <Fragment key={msg.date}>
                    <li key={msg.date} className="chat__messages-day">
                      {this.getDay(msg.timestamp)}
                    </li>
                    {msgView}
                  </Fragment>
                )
              }
              return msgView
            })}
          </ul>
        )}
        <div
          className={`chat__typing${
            activeChat?.typing ? ' chat__typing--show' : ''
          }`}
        >
          {activeChat?.typing}
        </div>

        <form className="chat-compose" onSubmit={this.onSubmit}>
          <button disabled={true} className="chat-attach">
            <Icon
              icon="ion-md-attach"
              style={{
                color: '#2A86F3',

                borderRadius: '50%',
                height: '22px',
                width: '22px',
                fontSize: '22px',
                lineHeight: 'inherit',
              }}
            />
          </button>
          <textarea
            onFocus={this.handleFocus}
            className="chat-compose__message-input"
            placeholder="Сообщение..."
            type="text"
            name="message"
            value={message}
            onChange={handleChange}
            autoComplete="off"
            ref={el => {
              this._input = el
            }}
          />

          <button disabled={true} className="chat-smilebutton">
            <Icon
              icon="ion-md-happy"
              style={{
                color: '#2A86F3',

                borderRadius: '50%',
                height: '22px',
                width: '22px',
                fontSize: '22px',
                lineHeight: 'inherit',
              }}
            />
          </button>

          {message.replace(/\n|\s/g, '') !== '' ? (
            <button
              type="submit"
              className="chat-compose__send-button"
              disabled={!message}
            >
              <Icon
                icon="ion-ios-arrow-round-up"
                style={{
                  color: 'white',
                  backgroundColor: '#2A86F3',
                  borderRadius: '50%',
                  height: '22px',
                  width: '22px',
                  minWidth: '22px',
                  fontSize: '21px',
                  lineHeight: '22px',
                }}
              />
            </button>
          ) : (
            <button className="chat-compose__send-button" disabled={!message}>
              <Icon
                icon="ion-md-mic"
                style={{
                  color: '#2A86F3',
                  borderRadius: '50%',
                  height: '22px',
                  width: '22px',
                  fontSize: '22px',
                  lineHeight: 'inherit',
                }}
              />
            </button>
          )}
        </form>
      </div>
    )
  }
}

export default Chat
