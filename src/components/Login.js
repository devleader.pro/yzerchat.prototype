import React from 'react'
import MaskedInput from 'react-text-mask'

import { Input, Button } from 'react-onsenui'

import rusFlagIcon from '../images/russia-flag.svg'

const Login = ({ phone, countryCode, handeSubmit, handleChange, setPhone }) => {
  return (
    <div className="loginPage">
      <div style={{ margin: 'auto' }}>
        <b
          onClick={() => {
            setPhone(' 995 997-23-11')
          }}
        >
          Phone number
        </b>

        <p style={{ fontSize: '14px', color: '#b1b1b1', padding: '0 40px' }}>
          Please select a country and enter the phone number to send
          verification code
        </p>

        <div className="loginPage__country-picker">
          <img src={rusFlagIcon} width="25" />
          <span>Russia</span>
        </div>

        <div style={{ borderBottom: '2px solid #2a86f3' }}>
          <div className="loginPage__phone-label">Phone</div>
          <div className="loginPage__phone-wrapper">
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {countryCode}
            </div>
            <MaskedInput
              className="loginPage__phone-input"
              showMask={false}
              guide={false}
              autoFocus
              value={phone}
              onChange={handleChange}
              type="tel"
              //prettier-ignore
              mask={[' ',/[1-9]/, /\d/, /\d/,' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, ]}
            />
          </div>
        </div>
      </div>

      <Button
        onClick={handeSubmit}
        style={{ marginTop: 'auto' }}
        disabled={phone.replace(/\s|-/g, '').length !== 10}
      >
        Get code
      </Button>
    </div>
  )
}

export default Login
