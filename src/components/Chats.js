import React, { useState } from 'react'
import { List, ListItem, Fab, Icon, Popover } from 'react-onsenui'
import Avatar from 'react-avatar'

import { getLastMsgDateTime } from '../utils'

const msgStatuses = ['unread', 'read']

const getLastMsgText = (chat, translates) => {
  if (chat.typing) {
    return (
      <div className="chats__typing">
        <span>&bull;</span>
        <span>&bull;</span>
        <span>&bull;</span>
        &nbsp;пишет...
      </div>
    )
  }
  const { active, lang } = chat.translate
  const { lastMessage } = chat
  if (lastMessage.type === 0 || !active) return lastMessage.text
  if (active && lastMessage.type === 1) {
    return translates[lang]?.[lastMessage.id] || lastMessage.text
  }
}

const getLastMsgStatus = msg => {
  if (msg.type === 0) {
    return (
      <span
        className={`chats_lastMsg-status chats_lastMsg-status--${
          msgStatuses[msg.status]
        }`}
      ></span>
    )
  }
  return null
}

const Chats = ({ openChat, chats, translates, newChat }) => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)
  let refs = null

  const filteredChats = chats.filter(chat => {
    return chat.lastMessage !== null
  })

  const sortedChats = filteredChats.sort((a, b) => {
    return b.lastMessage.timestamp - a.lastMessage.timestamp
  })

  return (
    <>
      <List
        dataSource={sortedChats}
        renderRow={(chat, idx) => {
          {
            return (
              chat.lastMessage !== null && (
                <ListItem
                  style={{ width: '100%' }}
                  tappable={true}
                  modifier="material"
                  onClick={() => openChat(chat.id)}
                  key={idx}
                >
                  <div className="left">
                    {chat.avatar ? (
                      <img className="list-item__thumbnail" src={chat.avatar} />
                    ) : (
                      <Avatar
                        name={chat.name}
                        size={40}
                        round="20px"
                        color={Avatar.getRandomColor(chat.name, [
                          '#3498db',
                          '#9b59b6',
                          '#e74c3c',
                          '#f1c40f',
                          '#2c3e50',
                          '#d35400',
                          '#1e3799',
                          '#b71540',
                        ])}
                      />
                    )}
                  </div>
                  <div className="center chat-preview">
                    <span className="list-item__title">{chat.name}</span>
                    {chat.lastMessage.text ? (
                      <span className="list-item__subtitle">
                        {getLastMsgText(chat, translates)}
                      </span>
                    ) : (
                      <span className="list-item__subtitle">sad</span>
                    )}
                  </div>
                  <div className="right chat-list__chat-meta">
                    <div className="last-msg-time">
                      {getLastMsgDateTime(chat.lastMessage.timestamp)}
                    </div>
                    {chat.unreadMessagesCount > 0 ? (
                      <div className="chat__unread-badge">
                        <span>{chat.unreadMessagesCount}</span>
                      </div>
                    ) : (
                      getLastMsgStatus(chat.lastMessage)
                    )}
                  </div>
                </ListItem>
              )
            )
          }
        }}
      />

      <Fab
        position="bottom right"
        ref={btn => {
          refs = btn
        }}
        onClick={() => setMenuIsOpen(true)}
        //style={{ position: 'fixed' }}
      >
        <Icon icon="ion-md-add" style={{ verticalAlign: 'middle' }}></Icon>
      </Fab>

      <Popover
        isOpen={menuIsOpen}
        onCancel={() => setMenuIsOpen(false)}
        getTarget={() => refs}
        maskColor="#71717159"
      >
        <List>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="ion-ios-megaphone, material:ion-md-megaphone"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div className="center strongblue">Новый канал</div>
          </ListItem>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="ion-md-people"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div className="center strongblue">Новая группа</div>
          </ListItem>
          <ListItem tappable={true} modifier="material">
            <div className="left strongblue">
              <Icon
                icon="md-comment-text-alt"
                size={26}
                fixedWidth={false}
                style={{ verticalAlign: 'middle' }}
              />
            </div>
            <div
              className="center strongblue"
              onClick={() => {
                setMenuIsOpen(false)
                newChat()
              }}
            >
              Новый чат
            </div>
          </ListItem>
        </List>
      </Popover>
    </>
  )
}

export default Chats
