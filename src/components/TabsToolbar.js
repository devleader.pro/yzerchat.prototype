import React from 'react'
import ons from 'onsenui'
import { Toolbar, Icon } from 'react-onsenui'

import logo from '../images/logo.png'

const getIOSToolbarTitle = tabIndex => {
  switch (tabIndex) {
    case 0:
      return 'Контакты'
    case 2:
      return 'Каналы'
    case 3:
      return 'Звонки'
    case 4:
      return 'Настройки'
    default:
      return (
        <img
          className="tabs-toolbar__logo"
          src={logo}
          width="112"
          height="44"
        />
      )
  }
}

const TabsToolbar = props => {
  const {
    showSearchPanel,
    term,
    handleChange,
    toggleSearchPanel,
    index,
  } = props

  const isAndroid = ons.platform.isAndroid()

  if (isAndroid) {
    return (
      <Toolbar>
        <div className="center overrided">
          {showSearchPanel ? (
            <div className="toolbar__search-wrapper">
              <div className="toolbar__search-panel">
                <div
                  type="text"
                  placeholder="search"
                  className="toolbar__search-input"
                >
                  <input
                    name="term"
                    type="text"
                    value={term}
                    onChange={handleChange}
                    autoFocus
                  />
                </div>
                <Icon
                  icon="ion-md-close"
                  size={22}
                  onClick={toggleSearchPanel}
                />
              </div>
            </div>
          ) : (
            <div className="toolbar__search-wrapper toolbar__search-wrapper--hide">
              <img
                className="tabs-toolbar__logo"
                src={logo}
                width="112"
                height="44"
              />

              <Icon
                style={{ position: 'absolute', right: '10px' }}
                icon="ion-md-search"
                size={22}
                onClick={toggleSearchPanel}
              />
            </div>
          )}
        </div>
      </Toolbar>
    )
  } else {
    return (
      <Toolbar>
        <div className="center tabs-toolbar tabs-toolbar--ios">
          {getIOSToolbarTitle(index)}
        </div>
      </Toolbar>
    )
  }
}

export default TabsToolbar
