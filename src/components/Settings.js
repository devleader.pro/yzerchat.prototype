import React from 'react'

import { List, ListHeader, ListItem, Icon, Fab } from 'react-onsenui'
import Avatar from 'react-avatar'

import SettingsEditNamePage from '../pages/SettingsEditNamePage'

const Settings = ({ profile, logout, setDisplayName, navigator }) => {
  const { id, name, email, phone, avatar } = profile
  return (
    <div>
      <div className="settings__top">
        <div className="settings__avatar">
          <Avatar
            src={avatar}
            name={name}
            round={true}
            style={{ fontSize: '24px' }}
            color={Avatar.getRandomColor(String(id), [
              '#3498db',
              '#9b59b6',
              '#e74c3c',
              '#f1c40f',
              '#2c3e50',
              '#d35400',
              '#1e3799',
              '#b71540',
            ])}
          />
        </div>
        <div
          className="settings__name"
          onClick={() => {
            setDisplayName('foo bar 2')
          }}
        >
          {name}
        </div>
      </div>
      <Fab
        position="top right"
        onClick={() => {
          navigator.pushPage(
            {
              component: SettingsEditNamePage,
              props: { key: 'SettingsEditNamePage', name: name },
            },
            { animation: 'slide' },
          )
        }}
      >
        <Icon icon="ion-md-create" style={{ verticalAlign: 'middle' }}></Icon>
      </Fab>
      <List className="list-left-icons">
        <ListHeader>Профиль</ListHeader>
        <ListItem tappable>
          <div className="left list-item__left">
            <Icon icon="ion-md-person" />
          </div>
          <div className="center">User ID</div>
          <div className="right list-item__right">
            {id && id.split(':chat.devlead.pro')[0]}
          </div>
        </ListItem>

        <ListItem tappable>
          <div className="left">
            <Icon icon="ion-md-call" />
          </div>
          <div className="center">Телефон</div>
          <div className="right">{phone || 'не добавлен'}</div>
        </ListItem>

        <ListItem tappable>
          <div className="left">
            <Icon icon="ion-md-mail" />
          </div>
          <div className="center">Email</div>
          <div className="right">{email || 'не добавлен'}</div>
        </ListItem>
      </List>
      <List style={{ marginTop: '10px' }} className="list-left-icons">
        <ListHeader>Настройки</ListHeader>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-notifications-none" />
          </div>
          <div className="center">Звуки и уведомления</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-translate" />
          </div>
          <div className="center">Настройки перевода</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-chart-donut" />
          </div>
          <div className="center">Хранилище и данные</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-shield-security" />
          </div>
          <div className="center">Безопасность</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="ion-md-image" />
          </div>
          <div className="center">Фон чатов</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-smartphone-android" />
          </div>
          <div className="center">Изменить номер телефона</div>
        </ListItem>
      </List>
      <List style={{ marginTop: '10px' }} className="list-left-icons">
        <ListHeader>Другое</ListHeader>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-comment-alert" />
          </div>
          <div className="center">Обратная связь</div>
        </ListItem>
        <ListItem tappable>
          <div className="left">
            <Icon icon="md-info-outline" />
          </div>
          <div className="center">О приложении</div>
        </ListItem>
      </List>
      <List
        style={{ marginTop: '10px', marginBottom: '10px' }}
        className="list-left-icons"
      >
        <ListItem tappable style={{ color: '#f64744' }} onClick={logout}>
          <div className="left">
            <div></div>
            <Icon icon="md-sign-in" />
          </div>
          <div className="center">Выход</div>
        </ListItem>
      </List>
    </div>
  )
}

export default Settings
