import React from 'react'

import { Input, Button } from 'react-onsenui'

const LoginConfirm = ({
  code,
  loginPhone,
  handeSubmit,
  handleChange,
  setCode,
}) => {
  const codeMask = ['—', '—', '—', '—', '—']
  code.split('').forEach((symbol, index) => {
    codeMask[index] = symbol
  })
  return (
    <div className="loginConfirmPage">
      <div style={{ margin: 'auto' }}>
        <b
          onClick={() => {
            setCode('12345')
          }}
        >
          Enter activation code
        </b>

        <p style={{ fontSize: '14px', color: '#b1b1b1' }}>
          We have sent the activation code to the phone number{' '}
          <b>{loginPhone}</b>
        </p>

        <div className="loginConfirmPage__code-mask">
          {codeMask.join('')}
          <input
            type="text"
            value={code}
            className="loginConfirmPage__code-input"
            onChange={handleChange}
            autoFocus
            maxLength="5"
            type="tel"
          />
        </div>
      </div>
      <Button
        onClick={handeSubmit}
        style={{ marginTop: 'auto' }}
        disabled={code.length < 5}
      >
        Log in
      </Button>
    </div>
  )
}

export default LoginConfirm
