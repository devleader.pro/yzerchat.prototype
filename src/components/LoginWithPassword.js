import React from 'react'

import { Input, Button } from 'react-onsenui'

const Login = ({
  user,
  password,
  handeSubmit,
  handleChange,
  setFakeLoginData,
}) => {
  return (
    <div className="loginPage">
      <b
        onClick={() => {
          setFakeLoginData('user1', 'cpUser1234')
        }}
      >
        User Id and Password
      </b>

      <p
        style={{ fontSize: '14px', color: '#b1b1b1' }}
        onClick={() => {
          setFakeLoginData('user2', 'cpUser1234')
        }}
      >
        Please enter User ID and password
      </p>

      <input
        placeholder="UserID"
        name="user"
        type="text"
        value={user}
        onChange={handleChange}
        className="login-input"
      />
      <input
        placeholder="Password"
        name="password"
        type="password"
        value={password}
        onChange={handleChange}
        className="login-input"
      />
      <Button
        onClick={handeSubmit}
        className="large-blue-button"
        style={{
          position: 'absolute',
          bottom: '1rem',
          left: '1rem',
          right: '1rem',
        }}
        disabled={!user && !password}
      >
        LOGIN
      </Button>
    </div>
  )
}

export default Login
