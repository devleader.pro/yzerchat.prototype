import React, { Component } from 'react'

import ons from 'onsenui'

import { Page, Tab, Tabbar } from 'react-onsenui'

import ContactsPage from '../pages/ContactsPage'
import ChatsPage from '../pages/ChatsPage'
import ChannelsPage from '../pages/ChannelsPage'
import CallsPage from '../pages/CallsPage'
import SettingsPage from '../pages/SettingsPage'

import TabsToolbar from '../components/TabsToolbar'

class RootTabs extends Component {
  state = {
    index: 1,
    showSearchPanel: false,
    term: '',
  }

  toggleSearchPanel = () => {
    const isOpen = this.state.showSearchPanel
    const term = this.state.term
    if (isOpen) {
      if (term.length > 0) {
        this.setState({ term: '' })
        return
      } else {
        this.setState({ showSearchPanel: false })
        return
      }
    } else {
      this.setState({ showSearchPanel: true })
      return
    }
  }

  handleChange = e => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  setIndex = index => {
    this.setState({ index })
  }

  newChat = () => {
    this.setIndex(0)
  }

  preChange = index => {
    if (ons.platform.isAndroid()) {
      this.setState({ index })
      return
    }
    this.setState({ index, term: '' })
  }

  renderTabs = () => {
    /**
    баг в Onsen UI:
    в таббаре пропадает иконка при изменении значения badge:
    https://github.com/OnsenUI/OnsenUI/issues/2693

    let chatsBadge = ''

    if (this.props.chats) {
      chatsBadge =
        this.props.chats.reduce((count, chat) => {
          return count + chat.unreadMessagesCount
        }, 0) || ''
    }

    */
    const { term } = this.state
    return [
      {
        content: (
          <ContactsPage
            key="ContactsPage"
            navigator={this.props.navigator}
            term={term}
            handleChange={this.handleChange}
          />
        ),
        tab: (
          <Tab
            key="ContactsPage"
            label="Контакты"
            icon="ion-ios-contact, material:ion-md-contact"
          />
        ),
      },
      {
        content: (
          <ChatsPage
            key="ChatsPage"
            navigator={this.props.navigator}
            newChat={this.newChat}
            term={term}
            handleChange={this.handleChange}
          />
        ),
        tab: <Tab key="ChatsPage" label="Чаты" icon="md-comment-text-alt" />,
      },
      {
        content: (
          <ChannelsPage
            key="ChannelsPage"
            navigator={this.props.navigator}
            newChat={this.newChat}
            term={term}
            handleChange={this.handleChange}
          />
        ),
        tab: (
          <Tab
            key="ChannelsPage"
            label="Каналы"
            icon="ion-ios-megaphone, material:ion-md-megaphone"
          />
        ),
      },
      {
        content: <CallsPage key="CallsPage" navigator={this.props.navigator} />,
        tab: (
          <Tab
            key="CallsPage"
            label="Звонки"
            icon="ion-ios-call, material:ion-md-call"
          />
        ),
      },
      {
        content: (
          <SettingsPage key="SettingsPage" navigator={this.props.navigator} />
        ),
        tab: (
          <Tab
            key="SettingsPage"
            label="Настройки"
            icon="ion-ios-settings, material:ion-md-settings"
          />
        ),
      },
    ]
  }

  /**
   * <ion-icon name="arrow-back"></ion-icon>
   * <ion-icon name="swap"></ion-icon>
   */

  render() {
    const { showSearchPanel, term, index } = this.state

    return (
      <Page
        renderToolbar={() => {
          return (
            <TabsToolbar
              showSearchPanel={showSearchPanel}
              term={term}
              handleChange={this.handleChange}
              toggleSearchPanel={this.toggleSearchPanel}
              index={index}
            />
          )
        }}
      >
        <Tabbar
          onPreChange={({ index }) => this.preChange(index)}
          renderTabs={this.renderTabs}
          index={index}
          animation="none"
          tabBorder={false}
        />
      </Page>
    )
  }
}

export default RootTabs
