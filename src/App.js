import React from 'react'
import { hot } from 'react-hot-loader/root'

import { connect } from 'react-redux'

import Root from './Root'

const App = () => {
  return <Root />
}

export default hot(connect()(App))
