// export const translate = (text, lang) => {
//   const key =
//     'trnsl.1.1.20190311T221540Z.f9eb30a8369a4820.e784b1fd737454eb18289dc5dca1261616218fa8'
//   text = encodeURI(text)
//   lang = lang
//   return fetch(
//     `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${key}&text=${text}&lang=${lang}`,
//     {
//       credentials: 'omit',
//       headers: {},
//       body: null,
//       method: 'GET',
//       mode: 'cors',
//     },
//   )
// }
import fetch from 'isomorphic-fetch'
class Translator {
  key =
    'trnsl.1.1.20190311T221540Z.f9eb30a8369a4820.e784b1fd737454eb18289dc5dca1261616218fa8'
  fetchParams = {
    credentials: 'omit',
    headers: {},
    body: null,
    method: 'GET',
    mode: 'cors',
  }

  async translate(text, lang) {
    const url = `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${
      this.key
    }&text=${encodeURI(text)}&lang=${lang}`
    const response = await fetch(url, this.fetchParams)
    const data = await response.json()
    return data.text[0]
  }
}

export default new Translator()

/* Пример использования

let result
translate('some shit', 'ru')
  .then(resp => resp.json())
  .then(body => (result = body))


result == {
    code: 200
    lang: "en-ru"
    text: ["какое-то дерьмо"]
    __proto__: Object
}
*/
