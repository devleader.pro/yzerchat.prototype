import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, Fab, Icon } from 'react-onsenui'

import Settings from '../components/Settings'
import { logout, setDisplayName } from '../store/actions'

class SettingsContainer extends Component {
  logout = () => {
    this.props.logout()
  }

  render() {
    return (
      <Page className="settings">
        <Settings
          profile={this.props.profile}
          logout={this.logout}
          setDisplayName={this.props.setDisplayName}
          navigator={this.props.navigator}
        />
      </Page>
    )
  }
}

export default connect(
  state => ({ profile: state.user.profile }),
  { logout, setDisplayName },
)(SettingsContainer)
