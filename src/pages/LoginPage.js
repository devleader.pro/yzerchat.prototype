import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page } from 'react-onsenui'

import Login from '../components/Login'
import LoginConfirmPage from './LoginConfirmPage'

class LoginContainer extends Component {
  state = {
    countryCode: '+7',
    phone: '',
  }

  handleChange = e => {
    this.setState({ phone: e.target.value })
  }

  handeSubmit = () => {
    const { countryCode, phone } = this.state
    this.props.navigator.pushPage({
      component: LoginConfirmPage,
      props: { key: 'LoginConfirmPage', phone: `${countryCode}${phone}` },
    })
  }

  setPhone = phone => {
    // fake
    this.setState({ phone })
  }

  render() {
    const { countryCode, phone } = this.state
    return (
      <Page>
        <Login
          phone={phone}
          countryCode={countryCode}
          handleChange={this.handleChange}
          handeSubmit={this.handeSubmit}
          setPhone={this.setPhone}
        />
      </Page>
    )
  }
}

export default connect(
  state => ({ user: state.user }),
  null,
)(LoginContainer)
