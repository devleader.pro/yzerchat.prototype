import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, Toolbar, BackButton, Toast } from 'react-onsenui'

import { attemptPasswordLogin, setError } from '../store/actions'

import LoginConfirm from '../components/LoginConfirm'

const fakeUsers = {
  t79999999999: 'srg1',
  t79999999991: 'user1',
  t79999999992: 'user2',
  t77777777771: 'test1',
  t77777777773: 'test3',
  t77777777774: 'test4',
  t77777777775: 'test5',
  t77777777776: 'test6',
  t77777777777: 'test7',
  t77777777778: 'test8',
  t77777777779: 'test9',
}

class LoginConfirmContainer extends Component {
  state = {
    code: '',
  }

  hideToast = () => {
    setTimeout(() => {
      this.props.setError('LOGIN_ERROR', false)
    }, 1500)
  }

  handleChange = e => {
    this.setState({ code: e.target.value })
  }

  handeSubmit = () => {
    let user = this.props.phone.replace(/\s|-/g, '').replace('+', 't')
    const fakeUser = fakeUsers[user]
    let password = this.state.code

    if (fakeUser) {
      user = fakeUser
      password = fakeUser === 'srg1' ? '12345' : 'cpUser1234'
    }

    this.props.attemptPasswordLogin(user, password)
  }

  setCode = code => {
    // fake
    this.setState({ code })
  }

  render() {
    return (
      <Page
        renderToolbar={() => (
          <Toolbar>
            <div className="left">
              <BackButton
                modifier={this.props.modifier}
                onClick={() => {
                  this.props.navigator.popPage()
                }}
              >
                Back
              </BackButton>
            </div>
          </Toolbar>
        )}
      >
        <Toast
          isOpen={this.props.error || false}
          onPostShow={this.hideToast}
          animation="fall"
          animationOptions={{ duration: 0.2, delay: 0.4, timing: 'ease-in' }}
        >
          <div>Ошибка! Попробуйте еще раз.</div>
        </Toast>
        <LoginConfirm
          code={this.state.code}
          loginPhone={this.props.phone.replace(/\s|-/g, '')}
          handleChange={this.handleChange}
          handeSubmit={this.handeSubmit}
          setCode={this.setCode}
        />
      </Page>
    )
  }
}

export default connect(
  state => ({ user: state.user, error: state.errors.loginError }),
  { attemptPasswordLogin, setError },
)(LoginConfirmContainer)
