import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Page,
  BackButton,
  Toolbar,
  Dialog,
  List,
  ListItem,
  Button,
  Switch,
  Icon,
} from 'react-onsenui'
import Avatar from 'react-avatar'
import Channel from '../components/Channel'
import { langs } from '../utils'

class ChannelPage extends Component {
  state = {
    message: '',
    langsIsOpent: false,
    avtoTranslate: false,
    choosenLang: '',
    avto: '',
  }
  componentDidMount() {
    this.setState({ avto: this.props.channel.translate.active })
  }

  chooseLang = lang => {
    this.setState({ choosenLang: lang.inits })
  }
  cancelLangs = () => {
    this.setState({ langsIsOpent: false })
  }
  avtoTranslateChange = e => {
    let checked = e.target.checked
    this.setState({ avtoTranslate: checked })
  }
  handleChange = e => {
    this.setState({ message: e.target.value })
  }
  handleSubmit = e => {
    e.preventDefault()
    const payload = { message: this.state.message }
    alert(JSON.stringify(payload, null, 2))
  }
  changeLanguage = () => {
    const payload = {}
    payload.lang = this.state.choosenLang
    payload.active = this.state.avtoTranslate
    this.setState(prevState => ({ avto: prevState.avtoTranslate }))
    this.cancelLangs()
  }
  render() {
    const { channelId, activeChannel, channel, message } = this.props
    const Body =
      !activeChannel || channelId !== activeChannel.id ? (
        <div className="chat">Загрузка</div>
      ) : (
        <Channel
          activeChannel={channel}
          message={message}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
      )
    return (
      <Page
        renderToolbar={() => (
          <Toolbar>
            <div className="left">
              <BackButton modifier={this.props.modifier}>Назад</BackButton>
            </div>
            <div className="center toolbar-width-center">
              <div className="topbar-center">
                {' '}
                <img
                  className="list-item__thumbnail channel-avatar"
                  src={channel.avatar}
                />{' '}
                <div className="topbar-channel-center">
                  <div>{channel.name}</div>
                  <div>{channel.members} участников</div>
                </div>
              </div>
            </div>
            <div className="right" style={{ margin: 'auto' }}>
              <div
                className="lang-circle"
                onClick={() => this.setState({ langsIsOpent: true })}
                style={
                  this.state.avto
                    ? { color: '#FFFFFF', backgroundColor: '#4098E0' }
                    : { color: '#999999', backgroundColor: '#F3F4F6' }
                }
              >
                <span>
                  {this.state.choosenLang.toUpperCase() ||
                    channel.translate.lang.toUpperCase()}
                </span>
              </div>
            </div>
            <Dialog
              onCancel={this.cancelLangs}
              isOpen={this.state.langsIsOpent}
              style={{ height: '100%' }}
              cancelable
            >
              <Page className="lang-page">
                <ListItem modifier="longdivider">Настройка перевода</ListItem>
                <ListItem modifier="longdivider">
                  <div className="left">Переводить автоматически</div>
                  <div className="right">
                    <Switch
                      defaultChecked={channel.translate.active}
                      checked={this.state.autoTranslate}
                      onChange={this.avtoTranslateChange}
                    />
                  </div>
                </ListItem>
                <List
                  style={{
                    overflowY: 'scroll',
                  }}
                  dataSource={langs}
                  renderRow={(lang, idx) => (
                    <ListItem
                      modifier="longdivider"
                      onClick={() => this.chooseLang(lang, idx)}
                      key={idx}
                    >
                      <div className="left" style={{ color: '#4098E0' }}>
                        {lang.inits.toUpperCase()}
                      </div>
                      {lang.name}
                      <div className="right">
                        {' '}
                        {this.state.choosenLang &&
                        this.state.choosenLang === lang.inits ? (
                          <Icon
                            icon="ion-md-checkmark-circle"
                            size={20}
                            style={{ color: '#4098E0' }}
                          />
                        ) : null}
                      </div>
                    </ListItem>
                  )}
                />

                <ListItem
                  modifier="nodivider"
                  style={{
                    marginTop: 'auto',
                    backgroundColor: 'white',
                  }}
                >
                  <div className="right">
                    <Button modifier="quiet" onClick={this.cancelLangs}>
                      отмена
                    </Button>
                    <Button modifier="quiet" onClick={this.changeLanguage}>
                      сохранить
                    </Button>
                  </div>
                </ListItem>
              </Page>
            </Dialog>
          </Toolbar>
        )}
      >
        {Body}
      </Page>
    )
  }
}

export default connect(state => ({
  activeChannel: state.activeChannel,
  channels: state.channels,
}))(ChannelPage)
