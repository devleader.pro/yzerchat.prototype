import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  toggleMessageShowTranslate,
  setChatTranslateSettings,
  closeActiveChat,
  scrollbackRoom,
} from '../store/actions'

import {
  Page,
  BackButton,
  Toolbar,
  Dialog,
  List,
  ListItem,
  Button,
  Switch,
  Icon,
} from 'react-onsenui'
import Avatar from 'react-avatar'
import { langs, debounce } from '../utils'
import Chat from '../components/Chat'

import MatrixClient from '../services/MatrixClient'

class ChatPage extends Component {
  state = {
    message: '',
    langsIsOpent: false,
    avtoTranslate: this.props.chat.translate.active,
    choosenLang: this.props.chat.translate.lang,
    manuallyTranslated: {},
  }

  loadHistory = debounce(() => {
    if (
      this.props.activeChat.paginationToken !== null &&
      !this.props.activeChat.loading
    ) {
      this.props.scrollbackRoom(this.props.chat.id)
    }
  }, 500)

  chooseLang = lang => {
    this.setState({ choosenLang: lang.inits })
  }

  cancelLangs = () => {
    this.setState({ langsIsOpent: false })
  }

  avtoTranslateChange = e => {
    let checked = e.target.checked
    this.setState({ avtoTranslate: checked })
  }

  _throttledTyping = debounce(
    () => MatrixClient.get().sendTyping(this.props.activeChat.id, true),
    10000,
  )

  handleChange = e => {
    const message = e.target.value
    if (message) {
      message.length === 1
        ? MatrixClient.get().sendTyping(this.props.activeChat.id, true)
        : this._throttledTyping()
    } else {
      MatrixClient.get().sendTyping(this.props.activeChat.id, false)
    }
    this.setState({ message: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    if (this.state.message.replace(/\n|\s/g, '') === '') return
    MatrixClient.get().sendTyping(this.props.activeChat.id, false)
    MatrixClient.sendTextMessage(this.props.chat.id, this.state.message).then(
      () => {
        this.setState({ message: '' })
      },
    )
  }

  changeLanguage = () => {
    this.props.setChatTranslateSettings(
      this.props.chat.id,
      this.state.choosenLang,
      this.state.avtoTranslate,
    )
    this.cancelLangs()
  }

  render() {
    const {
      chat,
      activeChat,
      toggleMessageShowTranslate,
      translates,
    } = this.props
    const { message } = this.state

    const Body =
      !activeChat || chat.id !== activeChat.id ? (
        <Chat
          activeChat={null}
          message={message}
          handleChange={this.handleChange}
        />
      ) : (
        <Chat
          activeChat={activeChat}
          chat={chat}
          message={message}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          toggleMessageShowTranslate={toggleMessageShowTranslate}
          translates={translates}
          loadHistory={this.loadHistory}
        />
      )

    return (
      <Page
        onDeviceBackButton={() => {
          this.props.closeActiveChat()
          this.props.navigator.popPage()
        }}
        renderToolbar={() => (
          <Toolbar>
            <div className="left">
              <BackButton
                modifier={this.props.modifier}
                onClick={() => {
                  this.props.closeActiveChat()
                  this.props.navigator.popPage()
                }}
              >
                Назад
              </BackButton>
            </div>
            <div className="center toolbar-width-center">{chat.name}</div>
            <div className="right" style={{ margin: 'auto' }}>
              <div
                className="lang-circle"
                onClick={() => this.setState({ langsIsOpent: true })}
                style={
                  chat.translate.active
                    ? { color: '#FFFFFF', backgroundColor: '#4098E0' }
                    : { color: '#999999', backgroundColor: '#F3F4F6' }
                }
              >
                <span>{chat.translate.lang.toUpperCase()}</span>
              </div>
            </div>
            <Dialog
              onCancel={this.cancelLangs}
              isOpen={this.state.langsIsOpent}
              style={{ height: '100%' }}
              cancelable
            >
              <Page className="lang-page">
                <ListItem modifier="longdivider">Настройка перевода</ListItem>
                <ListItem modifier="longdivider">
                  <div className="left">Переводить автоматически</div>
                  <div className="right">
                    <Switch
                      defaultChecked={chat.translate.active}
                      checked={this.state.avtoTranslate}
                      onChange={this.avtoTranslateChange}
                    />
                  </div>
                </ListItem>
                <List
                  style={{
                    overflowY: 'scroll',
                  }}
                  dataSource={langs}
                  renderRow={(lang, idx) => (
                    <ListItem
                      modifier="longdivider"
                      onClick={() => this.chooseLang(lang, idx)}
                      key={idx}
                    >
                      <div className="left" style={{ color: '#4098E0' }}>
                        {lang.inits.toUpperCase()}
                      </div>
                      {lang.name}
                      <div className="right">
                        {' '}
                        {this.state.choosenLang &&
                        this.state.choosenLang === lang.inits ? (
                          <Icon
                            icon="ion-md-checkmark-circle"
                            size={20}
                            style={{ color: '#4098E0' }}
                          />
                        ) : null}
                      </div>
                    </ListItem>
                  )}
                />
                <ListItem
                  modifier="nodivider"
                  style={{
                    marginTop: 'auto',
                    backgroundColor: 'white',
                  }}
                >
                  <div className="right">
                    <Button modifier="quiet" onClick={this.cancelLangs}>
                      отмена
                    </Button>
                    <Button modifier="quiet" onClick={this.changeLanguage}>
                      сохранить
                    </Button>
                  </div>
                </ListItem>
              </Page>
            </Dialog>
          </Toolbar>
        )}
      >
        {Body}
      </Page>
    )
  }
}

export default connect(
  (state, ownProps) => ({
    activeChat: state.activeChat,
    translates: state.translates,
    chat: state.chats.find(chat => {
      return chat.id === ownProps.chatId
    }),
  }),
  {
    toggleMessageShowTranslate,
    setChatTranslateSettings,
    closeActiveChat,
    scrollbackRoom,
  },
)(ChatPage)
