import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, SearchInput } from 'react-onsenui'
import ons from 'onsenui'

import Chats from '../components/Chats'
import ChatPage from '../pages/ChatPage'

import { setActiveChat } from '../store/actions'

import { searchInArrayByObjectProperty } from '../utils'

class ChatsContainer extends Component {
  openChat = chatId => {
    this.props.navigator
      .pushPage(
        {
          component: ChatPage,
          props: { key: 'ChatPage', chatId },
        },
        { animation: 'slide' },
      )
      .then(() => {
        this.props.setActiveChat(chatId)
      })
  }

  render() {
    const { chats, translates, newChat, term, handleChange } = this.props

    const filteredChats = searchInArrayByObjectProperty(chats, term)

    return (
      <Page>
        {!ons.platform.isAndroid() && (
          <div className="ios-search-input-wrapper">
            <SearchInput
              name="term"
              type="text"
              placeholder="Поиск"
              value={term}
              onChange={handleChange}
            />
          </div>
        )}
        <Chats
          openChat={this.openChat}
          chats={filteredChats}
          translates={translates}
          newChat={newChat}
        />
      </Page>
    )
  }
}

export default connect(
  state => ({
    chats: state.chats,
    translates: state.translates,
  }),
  { setActiveChat },
)(ChatsContainer)
