import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, Fab, Icon, SearchInput } from 'react-onsenui'

import ons from 'onsenui'

import { setActiveChat } from '../store/actions'

import Contacts from '../components/Contacts'
import ChatPage from '../pages/ChatPage'

import { searchInArrayByObjectProperty } from '../utils'

class ContactsContainer extends Component {
  openChat = chatId => {
    this.props.setActiveChat(chatId)
    this.props.navigator.pushPage(
      {
        component: ChatPage,
        props: { key: 'ChatPage', chatId },
      },
      { animation: 'slide' },
    )
  }

  render() {
    const { contacts, term, handleChange } = this.props

    const filteredContacts = searchInArrayByObjectProperty(contacts, term)

    return (
      <Page
        className="flex-page"
        renderFixed={() => (
          <Fab position="bottom right">
            <Icon icon="ion-md-add" style={{ verticalAlign: 'middle' }}></Icon>
          </Fab>
        )}
      >
        {!ons.platform.isAndroid() && (
          <div className="ios-search-input-wrapper">
            <SearchInput
              name="term"
              type="text"
              placeholder="Поиск"
              value={term}
              onChange={handleChange}
            />
          </div>
        )}
        <Contacts openChat={this.openChat} contacts={filteredContacts} />
      </Page>
    )
  }
}

export default connect(
  state => ({
    contacts: state.contacts,
    chats: state.chats,
  }),
  { setActiveChat },
)(ContactsContainer)
