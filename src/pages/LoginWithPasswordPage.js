import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page } from 'react-onsenui'

import LoginWithPassword from '../components/LoginWithPassword'
import { attemptPasswordLogin } from '../store/actions'

class LoginContainer extends Component {
  state = {
    user: '',
    password: '',
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handeSubmit = () => {
    const { user, password } = this.state
    this.props.attemptPasswordLogin(user, password)
  }

  setFakeLoginData = (user, password) => {
    // fake
    this.setState({ user, password })
  }

  render() {
    return (
      <Page>
        <LoginWithPassword
          user={this.state.user}
          password={this.state.password}
          handleChange={this.handleChange}
          handeSubmit={this.handeSubmit}
          setFakeLoginData={this.setFakeLoginData}
        />
      </Page>
    )
  }
}

export default connect(
  null,
  { attemptPasswordLogin },
)(LoginContainer)
