import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, Toolbar, BackButton, Icon, ToolbarButton } from 'react-onsenui'
import { setDisplayName } from '../store/actions'

import ons from 'onsenui'

const isAndroid = ons.platform.isAndroid()

class SettingsEditNameContainer extends Component {
  state = {
    name: this.props.name,
  }

  saveName = () => {
    const { name } = this.state
    if (name) {
      this.props.navigator.popPage().then(() => {
        if (name !== this.props.name) {
          this.props.setDisplayName(this.state.name)
        }
      })
    }
  }

  render() {
    return (
      <Page
        renderToolbar={() => (
          <Toolbar>
            <div className="left">
              <BackButton modifier={this.props.modifier}>Назад</BackButton>
            </div>
            <div
              className="center"
              style={{ fontSize: '16px', textAlign: 'center' }}
            >
              Имя
            </div>
            <div className="right" style={{ marginRight: '10px' }}>
              {isAndroid ? (
                <Icon
                  onClick={this.saveName}
                  icon="ion-md-checkmark"
                  style={{ verticalAlign: 'middle' }}
                ></Icon>
              ) : (
                <ToolbarButton onClick={this.saveName}>Save</ToolbarButton>
              )}
            </div>
          </Toolbar>
        )}
      >
        <div className="name-edit">
          <div className="name-edit__wrapper">
            <div className="name-edit__label">Ваше имя</div>
            <input
              className="name-edit__input"
              autoFocus
              type="text"
              value={this.state.name}
              onChange={e => {
                this.setState({ name: e.target.value })
              }}
            />
          </div>
        </div>
      </Page>
    )
  }
}

export default connect(
  null,
  { setDisplayName },
)(SettingsEditNameContainer)
