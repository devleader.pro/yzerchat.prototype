import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page, SearchInput } from 'react-onsenui'

import ons from 'onsenui'

import Channels from '../components/Channels'
import ChannelPage from './ChannelPage'

import { setActiveChannel } from '../store/actions'

import { searchInArrayByObjectProperty } from '../utils'

class ChannelsContainer extends Component {
  openChannel = (id, name, channel) => {
    this.props.navigator
      .pushPage(
        {
          component: ChannelPage,
          props: { key: 'ChannelPage', channelId: id, channel: channel },
        },
        { animation: 'slide' },
      )
      .then(() => {
        this.props.setActiveChannel(id)
      })
  }

  render() {
    const { channels, newChat, term, handleChange } = this.props

    const filteredChannels = searchInArrayByObjectProperty(channels, term)

    return (
      <Page>
        {!ons.platform.isAndroid() && (
          <div className="ios-search-input-wrapper">
            <SearchInput
              name="term"
              type="text"
              placeholder="Поиск"
              value={term}
              onChange={handleChange}
            />
          </div>
        )}
        <Channels
          openChannel={this.openChannel}
          channels={filteredChannels}
          newChat={newChat}
        />
      </Page>
    )
  }
}

export default connect(
  state => ({ channels: state.channels }),
  { setActiveChannel },
)(ChannelsContainer)
