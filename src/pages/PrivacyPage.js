import React from 'react'
import { connect } from 'react-redux'

import { acceptPrivacy } from '../store/actions'

import { Button, Page } from 'react-onsenui'
import yzerlogo from '../images/icon-yzer.svg'
import PrivacyPolicy from '../components/PrivacyPolicy'

const PrivacyPage = ({ acceptPrivacy, navigator }) => {
  const showPolicy = () => {
    navigator.pushPage(
      {
        component: PrivacyPolicy,
        props: { key: 'PrivacyPolicy' },
      },
      { animation: 'slide' },
    )
  }

  return (
    <Page>
      <div className="privacy">
        <div className="privacy-header">
          <img src={yzerlogo} className="privacy-img" width="35%" />
          <p>YZER</p>
          <span>Speak freely with the whole world</span>
        </div>
        <div className="privacy-bottom-block">
          <span>
            By start using YZER, you agree
            <br /> to the{' '}
            <a onClick={showPolicy}>Privacy Policy and Terms of Service.</a>
          </span>
          <Button
            className="large-blue-button"
            modifier="large--cta"
            type="button"
            onClick={acceptPrivacy}
          >
            I AGREE
          </Button>
        </div>
      </div>
    </Page>
  )
}

export default connect(
  null,
  { acceptPrivacy },
)(PrivacyPage)
