import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Page } from 'react-onsenui'

import Calls from '../components/Calls'

class CallsContainer extends Component {
  render() {
    return (
      <Page>
        <Calls />
      </Page>
    )
  }
}

export default connect()(CallsContainer)
