process.traceDeprecation = true
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const mergeWith = require('lodash/mergeWith')
const isArray = require('lodash/isArray')

const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 3000
const sourceDir = 'src'
const publicPath = process.env.PUBLIC_PATH || '/'
const sourcePath = path.join(process.cwd(), sourceDir)
const outputPath = path.join(process.cwd(), 'www')

function customizer(objValue, srcValue) {
  if (isArray(objValue)) {
    return objValue.concat(srcValue)
  }
  return undefined
}

const wpConfig = {
  base: {
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: ['thread-loader', 'babel-loader'],
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.s[ac]ss$/i,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.(jpe?g|woff2?|ttf|eot)$/,
          loader: 'url-loader?limit=8000',
          
        },
        
        { test: /\.(png|svg)$/i, loader: 'file-loader',
				
				options: {
					name: '[name].[ext]',
					path: outputPath,
        			publicPath: publicPath,
				}},
      ],
    },

    plugins: [
      new webpack.ProgressPlugin(),

      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: path.join(process.cwd(), 'public/index.html'),
      }),

      new webpack.DefinePlugin({
        NODE_ENV: process.env.NODE_ENV,
        PUBLIC_PATH: publicPath.replace(/\/$/, ''),
      }),

      new CopyWebpackPlugin([
        {
          from: sourcePath+'/appimgs/',
          to: outputPath+'/res/'
        },
      ]),
    ],

    resolve: {
      extensions: ['.js', '.jsx', '.json'],
      modules: [].concat(sourceDir, ['node_modules']),
    },

    entry: {
      app: [sourcePath],
    },

    output: {
      filename: '[name].js',
      path: outputPath,
      publicPath,
    },
  },

  development: {
    mode: 'development',

    plugins: [
      new webpack.HotModuleReplacementPlugin({
        fullBuildTimeout: 200,
      }),
    ],

    entry: {
      app: ['webpack/hot/only-dev-server'],
    },

    devtool: 'cheap-module-source-map',

    devServer: {
      hot: true,
      historyApiFallback: { index: publicPath },
      inline: true,
      contentBase: 'public',
      headers: { 'Access-Control-Allow-Origin': '*' },
      host,
      port,
      stats: 'errors-only',
    },

    optimization: {
      namedModules: true,
    },
  },

  production: {
    mode: 'production',

    output: {
      filename: '[name].[chunkhash:4].js',
    },

    optimization: {
      minimizer: [
        new TerserPlugin({
          cache: false,
          parallel: true,
          sourceMap: true,
          extractComments: true,
        }),
      ],
      splitChunks: {
        name: 'vendor',
        minChunks: 2,
      },
    },
  },
}

const config = mergeWith(
  {},
  wpConfig.base,
  wpConfig[process.env.NODE_ENV],
  customizer,
)

module.exports = config
